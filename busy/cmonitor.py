#!/usr/bin/env python3

import random
import time
import logging
import uuid
import numpy.random


def config_log():
    log = logging.getLogger('cmonitor')
    f = logging.Formatter('%(asctime)s - %(name)s:%(process)d - %(levelname)s - %(message)s')
    h = logging.StreamHandler()
    h.setLevel(logging.DEBUG)
    h.setFormatter(f)
    log.addHandler(h)
    log.setLevel(logging.DEBUG)
    return log


def mon_cpu():
    s = ['0x283FE8', '#AFEFEF', '2398098098']
    return numpy.random.choice(s)


def mon_slice():
    s = random.random()
    c = [
        'Created slice User Slice of root.',
        'Starting User Slice of root.',
        'Started Session {} of user root.'.format(s),
        'Starting Session {} of user root.'.format(s),
        'Removed slice User Slice of root.']
    return numpy.random.choice(c)


def monitor(log):
    while True:
        time.sleep(random.random())
        xsel = [1, 2, 3, 4, 5, 6]
        xwei = [0.3, 0.05, 0.1, 0.3, 0.2, 0.05]
        x = numpy.random.choice(xsel, p=xwei)
        if x == 1:
            log.info('process {}'.format(uuid.uuid4()))
        elif x == 2:
            log.warning('interrupt 0x93')
        elif x == 3:
            log.error('internal register corruption: 0x4343d3')
        elif x == 4:
            log.debug(mon_slice())
        elif x == 5:
            log.debug('register: {}'.format(numpy.random.bytes(16)))
        else:
            log.info('continue processing on to next slice')
        time.sleep(random.random() / 2)


if __name__ == "__main__":
    log = config_log()
    sessionid = uuid.uuid4()
    log.info('cmonitor starting {}'.format(sessionid))
    try:
        monitor(log)
    except KeyboardInterrupt as e:
        log.info('KeyboardInterrupt {}'.format(e))
